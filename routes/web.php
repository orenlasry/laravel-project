<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::resource('jobs', 'JobsController')->middleware('auth');

Auth::routes();

Route::get('jobs/delete/{id}','JobsController@destroy')->name('job.delete');

Route::get('/openOrders','JobsController@openOrders')->name('jobs.openOrders')->middleware('auth');

Route::resource('users', 'UsersController')->middleware('auth');

Route::resource('customers', 'CustomersController')->middleware('auth');

Route::get('users/delete/{id}','UsersController@destroy')->name('user.delete');

Route::get('customers/delete/{id}','CustomersController@destroy')->name('customers.delete');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('jobs/changestatus/{jid}/{sid}','JobsController@changeStatus')->name('job.changestatus');

Route::get('jobs/changeuser/{jid}/{uid?}','JobsController@changeUser')->name('job.changeuser');

Route::get('/verify','Auth\RegisterController@verifyUser')->name('verify.user');

Route::get('jobs/download/{id}', 'JobsController@download')->name('job.pdf');

Route::get('service', 'PagesController@getContact')->name('pages.service');

Route::post('service', 'PagesController@postContact');

Route::get('users/userprofile/{id}', 'UsersController@userProfile')->name('users.profile')->middleware('auth');


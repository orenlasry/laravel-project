Hello {{$email_data['name']}}
<br><br>
Welcome to Elraz Furniture Industries Ltd System!
<br>
Please click the below link to verify your email and activate your account!
<br><br>
<a href="http://localhost/project/public/verify?code={{$email_data['verification_code']}}">Click Here!</a>

<br><br>
Thank you!
<br>
Elraz Furniture Industries Ltd

<br><br>
Note: This is an automated message, please do not replay to this email
@extends('layouts.newhome')

@section('title', 'Edit user')

@section('content')       
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit user</div>        
                    <div class="card-body">
                        <form method = "post" action = "{{action('UsersController@update',$user->id)}}">
                        @csrf
                        @METHOD('PATCH')
                        <div class="form-group">
                            <label for = "name">Username</label>
                            <input type = "text" class="form-control" name = "name" value = "{{$user->name}}">
                        </div>     
                        <div class="form-group">
                            <label for = "email">User email</label>
                            <input type = "text" class="form-control" name = "email" value = {{$user->email}}>
                        </div>
                        <div>
                            <input type = "submit" name = "submit" value = "Update user">
                        </div>                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

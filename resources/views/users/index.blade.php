@extends('layouts.newhome')

@section('title', 'Users')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }} </div>
    @endif
    <div><a class="btn btn-primary active" href="{{ route('users.create') }}">Add new User</a></div>
    <h1>Users List</h1>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Edit User</th>
                    <th>Delete User</th>
                </tr>
                <!-- table data -->
            </thead>
            <tbody class="text-white font-weight-bold">
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @foreach ($user->roles as $role)
                                {{ $role->name }}
                            @endforeach
                        </td>
                        <td>{{ $user->created_at }}</td>
                        <td>{{ $user->updated_at }}</td>
                        <td><a class="btn btn-outline-dark text-white font-weight-bold"
                                href="{{ route('users.edit', $user->id) }}" role="button">Edit</a></td>
                        <td><a class="btn btn-outline-danger text-white font-weight-bold"
                                href="{{ route('user.delete', $user->id) }}" role="button">Delete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

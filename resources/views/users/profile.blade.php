@extends('layouts.newhome')

@section('title', 'Profile')

@section('content')
    <table class="table" style="margin-top:5%">
        <thead class="thead-dark">
            <tr>
                <th>Name :</th>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <th>Email :</th>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <th>Creation :</th>
                <td>{{ $user->created_at->toDateString() }}</td>
            </tr>
        </thead>
    </table>
@endsection

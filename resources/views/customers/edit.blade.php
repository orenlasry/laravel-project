@extends('layouts.newhome')

@section('title','Edit Customer')

@section('content')
    <h1>Edit Customer number : {{$customer->customer_number}}</h1>
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method = "post" action = "{{action('CustomersController@update',$customer->id)}}">
        @csrf <!-- protect from csrf attack -->
        @method('PATCH')
        <div class="form-group" >
            <label for = "customer_number">Customer Number</label>
            <input type = "number" class="form-control" name = "customer_number" value="{{$customer->customer_number}}"">
        </div>
        <div class="form-group" >
            <label for = "name">Name</label>
            <input type = "text" class="form-control" name = "name" value="{{$customer->name}}">
        </div>
        <div class="form-group" >
            <label for = "phone_num">Phone Num</label>
            <input type = "number" class="form-control" name = "phone_num" value={{$customer->phone_num}}>
        </div>
        <div class="form-group" >
            <label for = "email">Email</label>
            <input type = "text" class="form-control" name = "email" value={{$customer->email}}>
        </div>
        <div class="form-group" >
            <label for = "address">Address</label>
            <input type = "text" class="form-control" name = "address" value={{$customer->address}}>
        </div>
        <div>
            <input class="btn btn-outline-primary" type = "submit" name = "submit" value = "Update Customer">
        </div>
    </form>
@endsection

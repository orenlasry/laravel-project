@extends('layouts.newhome')

@section('title','Create Customer')

@section('content')
    <body>
        <h1>create Customer</h1>
        <form method = "post" action = "{{action('CustomersController@store')}}">
            @csrf
            <div class="form-group" >
                <label for = "customer_number">Customer Number</label>
                <input type = "number" class="form-control" name = "customer_number">
            </div>
            <div class="form-group" >
                <label for = "name">Name</label>
                <input type = "text" class="form-control" name = "name">
            </div>
            <div class="form-group" >
                <label for = "phone_num">Phone Number</label>
                <input type = "tel" class="form-control" name = "phone_num" placeholder="05x-xxxxxxx" pattern="[0-9]{3}[0-9]{7}" required> 
            </div>
            <div class="form-group" >
                <label for = "email">Email</label>
                <input type = "email" class="form-control" name = "email">
            </div>
            <div class="form-group" >
                <label for = "address">Address</label>
                <input type = "text" class="form-control" name = "address">
            </div>
            @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
             @endforeach
            </ul>
            </div>
        @endif
        <div class="form-group">
            <input type = "submit" class="form-control" name = "submit" value = "Create Customer">
        </div>
        </form>
    </body>
@endsection

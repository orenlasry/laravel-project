@extends('layouts.newhome')

@section('title', 'Customers')

@section('content')

    @if (Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{ Session::get('notallowed') }}
        </div>

    @endif

    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }} </div>
    @endif
    <div><a class="btn btn-primary active" href="{{ route('customers.create') }}">Add new Customer</a></div>

    <h1>Customers</h1>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>Customer Number</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Edit Customer</th>
                    <th>Delete Customer</th>
                </tr>
            </thead>
            <tbody class="text-white font-weight-bold">
                <!-- the table data -->
                @foreach ($customers as $customer)
                    <tr>
                        <td> {{ $customer->customer_number }}</td>
                        <td> {{ $customer->name }}</td>
                        <td> {{ str_pad($customer->phone_num, 10, '0', STR_PAD_LEFT) }}</td>
                        <td> {{ $customer->email }}</td>
                        <td> {{ $customer->address }}</td>
                        <td> {{ $customer->created_at }}</td>
                        <td> {{ $customer->updated_at }}</td>
                        <td><a class="btn btn-outline-dark text-white font-weight-bold"
                                href="{{ route('customers.edit', $customer->id) }}" role="button">Edit</a></td>
                        <td><a class="btn btn-outline-danger" data-toggle="modal" data-target="#delete_customer" role="button">Delete</a></td>
                        <!-- begin:modal Delete Customer -->
                        <div id="delete_customer" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <center>
                                        <div class="modal-header">
                                            <h3 class="modal-title">Confirmation</h3><br>
                                        </div>
                                    </center>
                                    <div class="modal-body">
                                        <p>
                                            Are you sure want to Delete this Order?
                                        </p>
                                        <form class="form-horizontal" method="get"
                                            action="{{ route('customers.delete', $customer->id) }}">
                                            {{ csrf_field() }}
                                            <br>
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <button type="button"
                                                        class="btn btn-hover btn-primary btn-sm text-white font-weight-bold"
                                                        data-dismiss="modal">Cancel</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="btn btn btn-hover btn-danger btn-sm" type="submit"
                                                        value="Yes, Delete">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End:modal Delete Customer -->
                    </tr>
                @endforeach
            </tbody>
        </table>


    @endsection

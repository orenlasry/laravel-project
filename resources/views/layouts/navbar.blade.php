<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0 text-white">Elraz Furniture Industries Ltd.</a>
    <div class="navbar-brand pt-0">
    </div>
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <div class="btn btn-dark">
              <a class="btn btn-dark" href="{{ route('users.profile', Auth::user()->id) }}">
                <span class="no-icon">Account</span>
            </a>
                <a class="btn btn-dark" href="{{ route('logout') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
                <b>
                    @if (!Auth::guest())
                        {{ Auth::user()->name }}
                    @endif
                </b>
            </div>
        </li>
    </ul>
</nav>

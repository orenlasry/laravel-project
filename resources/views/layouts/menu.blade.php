{{-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ----------> --}}

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>

<style>


dd {
    margin-right: 0;
}

figure {
    margin: 0 0 1rem;
}

caption {
    text-align: right;
}

th {
    text-align: right;
}

.list-unstyled {
    padding-right: 0;
}

.list-inline {
    padding-right: 0;
}

.list-inline-item:not(:last-child) {
    margin-right: 0;
    margin-left: 5px;
}

.blockquote {
    border-left: none;
    border-right: 0.25rem solid #eceeef;
}

.blockquote-reverse {
    padding-left: 1rem;
    padding-right: 0;
    text-align: left;
    border-left: 0.25rem solid #eceeef;
    border-right: 0;
}

dl.row > dd + dt {
    clear: right;
}


.pull-0 {
    left: auto;
}

.pull-1 {
    left: 8.33333%;
}

.pull-2 {
    left: 16.66667%;
}

.pull-3 {
    left: 25%;
}

.pull-4 {
    left: 33.33333%;
}

.pull-5 {
    left: 41.66667%;
}

.pull-6 {
    left: 50%;
}

.pull-7 {
    left: 58.33333%;
}

.pull-8 {
    left: 66.66667%;
}

.pull-9 {
    left: 75%;
}

.pull-10 {
    left: 83.33333%;
}

.pull-11 {
    left: 91.66667%;
}

.pull-12 {
    left: 100%;
}

.push-0 {
    right: auto;
}

.push-1 {
    right: 8.33333%;
}

.push-2 {
    right: 16.66667%;
}

.push-3 {
    right: 25%;
}

.push-4 {
    right: 33.33333%;
}

.push-5 {
    right: 41.66667%;
}

.push-6 {
    right: 50%;
}

.push-7 {
    right: 58.33333%;
}

.push-8 {
    right: 66.66667%;
}

.push-9 {
    right: 75%;
}

.push-10 {
    right: 83.33333%;
}

.push-11 {
    right: 91.66667%;
}

.push-12 {
    right: 100%;
}

.offset-1 {
    margin-right: 8.33333%;
}

.offset-2 {
    margin-right: 16.66667%;
}

.offset-3 {
    margin-right: 25%;
}

.offset-4 {
    margin-right: 33.33333%;
}

.offset-5 {
    margin-right: 41.66667%;
}

.offset-6 {
    margin-right: 50%;
}

.offset-7 {
    margin-right: 58.33333%;
}

.offset-8 {
    margin-right: 66.66667%;
}

.offset-9 {
    margin-right: 75%;
}

.offset-10 {
    margin-right: 83.33333%;
}

.offset-11 {
    margin-right: 91.66667%;
}

@media (min-width: 544px) {

    .pull-sm-0 {
        left: auto;
    }

    .pull-sm-1 {
        left: 8.33333%;
    }

    .pull-sm-2 {
        left: 16.66667%;
    }

    .pull-sm-3 {
        left: 25%;
    }

    .pull-sm-4 {
        left: 33.33333%;
    }

    .pull-sm-5 {
        left: 41.66667%;
    }

    .pull-sm-6 {
        left: 50%;
    }

    .pull-sm-7 {
        left: 58.33333%;
    }

    .pull-sm-8 {
        left: 66.66667%;
    }

    .pull-sm-9 {
        left: 75%;
    }

    .pull-sm-10 {
        left: 83.33333%;
    }

    .pull-sm-11 {
        left: 91.66667%;
    }

    .pull-sm-12 {
        left: 100%;
    }

    .push-sm-0 {
        right: auto;
    }

    .push-sm-1 {
        right: 8.33333%;
    }

    .push-sm-2 {
        right: 16.66667%;
    }

    .push-sm-3 {
        right: 25%;
    }

    .push-sm-4 {
        right: 33.33333%;
    }

    .push-sm-5 {
        right: 41.66667%;
    }

    .push-sm-6 {
        right: 50%;
    }

    .push-sm-7 {
        right: 58.33333%;
    }

    .push-sm-8 {
        right: 66.66667%;
    }

    .push-sm-9 {
        right: 75%;
    }

    .push-sm-10 {
        right: 83.33333%;
    }

    .push-sm-11 {
        right: 91.66667%;
    }

    .push-sm-12 {
        right: 100%;
    }

    .offset-sm-0 {
        margin-right: 0%;
    }

    .offset-sm-1 {
        margin-right: 8.33333%;
    }

    .offset-sm-2 {
        margin-right: 16.66667%;
    }

    .offset-sm-3 {
        margin-right: 25%;
    }

    .offset-sm-4 {
        margin-right: 33.33333%;
    }

    .offset-sm-5 {
        margin-right: 41.66667%;
    }

    .offset-sm-6 {
        margin-right: 50%;
    }

    .offset-sm-7 {
        margin-right: 58.33333%;
    }

    .offset-sm-8 {
        margin-right: 66.66667%;
    }

    .offset-sm-9 {
        margin-right: 75%;
    }

    .offset-sm-10 {
        margin-right: 83.33333%;
    }

    .offset-sm-11 {
        margin-right: 91.66667%;
    }
}

@media (min-width: 768px) {

    .pull-md-0 {
        left: auto;
    }

    .pull-md-1 {
        left: 8.33333%;
    }

    .pull-md-2 {
        left: 16.66667%;
    }

    .pull-md-3 {
        left: 25%;
    }

    .pull-md-4 {
        left: 33.33333%;
    }

    .pull-md-5 {
        left: 41.66667%;
    }

    .pull-md-6 {
        left: 50%;
    }

    .pull-md-7 {
        left: 58.33333%;
    }

    .pull-md-8 {
        left: 66.66667%;
    }

    .pull-md-9 {
        left: 75%;
    }

    .pull-md-10 {
        left: 83.33333%;
    }

    .pull-md-11 {
        left: 91.66667%;
    }

    .pull-md-12 {
        left: 100%;
    }

    .push-md-0 {
        right: auto;
    }

    .push-md-1 {
        right: 8.33333%;
    }

    .push-md-2 {
        right: 16.66667%;
    }

    .push-md-3 {
        right: 25%;
    }

    .push-md-4 {
        right: 33.33333%;
    }

    .push-md-5 {
        right: 41.66667%;
    }

    .push-md-6 {
        right: 50%;
    }

    .push-md-7 {
        right: 58.33333%;
    }

    .push-md-8 {
        right: 66.66667%;
    }

    .push-md-9 {
        right: 75%;
    }

    .push-md-10 {
        right: 83.33333%;
    }

    .push-md-11 {
        right: 91.66667%;
    }

    .push-md-12 {
        right: 100%;
    }

    .offset-md-0 {
        margin-right: 0%;
    }

    .offset-md-1 {
        margin-right: 8.33333%;
    }

    .offset-md-2 {
        margin-right: 16.66667%;
    }

    .offset-md-3 {
        margin-right: 25%;
    }

    .offset-md-4 {
        margin-right: 33.33333%;
    }

    .offset-md-5 {
        margin-right: 41.66667%;
    }

    .offset-md-6 {
        margin-right: 50%;
    }

    .offset-md-7 {
        margin-right: 58.33333%;
    }

    .offset-md-8 {
        margin-right: 66.66667%;
    }

    .offset-md-9 {
        margin-right: 75%;
    }

    .offset-md-10 {
        margin-right: 83.33333%;
    }

    .offset-md-11 {
        margin-right: 91.66667%;
    }
}

@media (min-width: 992px) {

    .pull-lg-0 {
        left: auto;
    }

    .pull-lg-1 {
        left: 8.33333%;
    }

    .pull-lg-2 {
        left: 16.66667%;
    }

    .pull-lg-3 {
        left: 25%;
    }

    .pull-lg-4 {
        left: 33.33333%;
    }

    .pull-lg-5 {
        left: 41.66667%;
    }

    .pull-lg-6 {
        left: 50%;
    }

    .pull-lg-7 {
        left: 58.33333%;
    }

    .pull-lg-8 {
        left: 66.66667%;
    }

    .pull-lg-9 {
        left: 75%;
    }

    .pull-lg-10 {
        left: 83.33333%;
    }

    .pull-lg-11 {
        left: 91.66667%;
    }

    .pull-lg-12 {
        left: 100%;
    }

    .push-lg-0 {
        right: auto;
    }

    .push-lg-1 {
        right: 8.33333%;
    }

    .push-lg-2 {
        right: 16.66667%;
    }

    .push-lg-3 {
        right: 25%;
    }

    .push-lg-4 {
        right: 33.33333%;
    }

    .push-lg-5 {
        right: 41.66667%;
    }

    .push-lg-6 {
        right: 50%;
    }

    .push-lg-7 {
        right: 58.33333%;
    }

    .push-lg-8 {
        right: 66.66667%;
    }

    .push-lg-9 {
        right: 75%;
    }

    .push-lg-10 {
        right: 83.33333%;
    }

    .push-lg-11 {
        right: 91.66667%;
    }

    .push-lg-12 {
        right: 100%;
    }

    .offset-lg-0 {
        margin-right: 0%;
    }

    .offset-lg-1 {
        margin-right: 8.33333%;
    }

    .offset-lg-2 {
        margin-right: 16.66667%;
    }

    .offset-lg-3 {
        margin-right: 25%;
    }

    .offset-lg-4 {
        margin-right: 33.33333%;
    }

    .offset-lg-5 {
        margin-right: 41.66667%;
    }

    .offset-lg-6 {
        margin-right: 50%;
    }

    .offset-lg-7 {
        margin-right: 58.33333%;
    }

    .offset-lg-8 {
        margin-right: 66.66667%;
    }

    .offset-lg-9 {
        margin-right: 75%;
    }

    .offset-lg-10 {
        margin-right: 83.33333%;
    }

    .offset-lg-11 {
        margin-right: 91.66667%;
    }
}

@media (min-width: 1200px) {

    .pull-xl-0 {
        left: auto;
    }

    .pull-xl-1 {
        left: 8.33333%;
    }

    .pull-xl-2 {
        left: 16.66667%;
    }

    .pull-xl-3 {
        left: 25%;
    }

    .pull-xl-4 {
        left: 33.33333%;
    }

    .pull-xl-5 {
        left: 41.66667%;
    }

    .pull-xl-6 {
        left: 50%;
    }

    .pull-xl-7 {
        left: 58.33333%;
    }

    .pull-xl-8 {
        left: 66.66667%;
    }

    .pull-xl-9 {
        left: 75%;
    }

    .pull-xl-10 {
        left: 83.33333%;
    }

    .pull-xl-11 {
        left: 91.66667%;
    }

    .pull-xl-12 {
        left: 100%;
    }

    .push-xl-0 {
        right: auto;
    }

    .push-xl-1 {
        right: 8.33333%;
    }

    .push-xl-2 {
        right: 16.66667%;
    }

    .push-xl-3 {
        right: 25%;
    }

    .push-xl-4 {
        right: 33.33333%;
    }

    .push-xl-5 {
        right: 41.66667%;
    }

    .push-xl-6 {
        right: 50%;
    }

    .push-xl-7 {
        right: 58.33333%;
    }

    .push-xl-8 {
        right: 66.66667%;
    }

    .push-xl-9 {
        right: 75%;
    }

    .push-xl-10 {
        right: 83.33333%;
    }

    .push-xl-11 {
        right: 91.66667%;
    }

    .push-xl-12 {
        right: 100%;
    }

    .offset-xl-0 {
        margin-right: 0%;
    }

    .offset-xl-1 {
        margin-right: 8.33333%;
    }

    .offset-xl-2 {
        margin-right: 16.66667%;
    }

    .offset-xl-3 {
        margin-right: 25%;
    }

    .offset-xl-4 {
        margin-right: 33.33333%;
    }

    .offset-xl-5 {
        margin-right: 41.66667%;
    }

    .offset-xl-6 {
        margin-right: 50%;
    }

    .offset-xl-7 {
        margin-right: 58.33333%;
    }

    .offset-xl-8 {
        margin-right: 66.66667%;
    }

    .offset-xl-9 {
        margin-right: 75%;
    }

    .offset-xl-10 {
        margin-right: 83.33333%;
    }

    .offset-xl-11 {
        margin-right: 91.66667%;
    }
}

.form-check-label {
    padding-right: 1.25rem;
}

.form-check-input {
    margin-right: -1.25rem;
}

.form-check-inline {
    padding-right: 1.25rem;
}

    .form-check-inline + .form-check-inline {
        margin-right: .75rem;
    }

.form-control-feedback {
    margin-top: 0.25rem;
}

.form-control-success,
.form-control-warning,
.form-control-danger {
    padding-left: 2.25rem;
    background-position: center right 0.625rem;
}

.form-inline .form-check-label {
    padding-right: 0;
}

.dropdown-toggle::after {
    margin-right: 0.3em;
}

.dropdown-menu {
    right: 0;
    float: right;
    margin: 2px 0 0;
    text-align: right;
}

.dropdown-divider {
    margin: 0.5rem 0;
}



.btn-group .btn + .btn,
.btn-group .btn + .btn-group,
.btn-group .btn-group + .btn,
.btn-group .btn-group + .btn-group {
    margin-right: -1px;
}

.btn-toolbar {
    margin-right: -0.5rem;
}



    .btn-toolbar > .btn,
    .btn-toolbar > .btn-group,
    .btn-toolbar > .input-group {
        margin-right: 0.5rem;
    }

.btn-group > .btn:first-child {
    margin-right: 0;
}

.btn-group > .btn-group {
    float: right;
}

.btn + .dropdown-toggle-split::after {
    margin-right: 0;
}


.btn-group-vertical > .btn + .btn,
.btn-group-vertical > .btn + .btn-group,
.btn-group-vertical > .btn-group + .btn,
.btn-group-vertical > .btn-group + .btn-group {
    margin-right: 0;
}

.input-group .form-control {
    float: left;
}

.input-group-addon:not(:last-child) {
    border-right: 1px solid rgba(0,0,0,.15);
    border-left: 0;
}

.form-control + .input-group-addon:not(:first-child) {
    border-left-width: medium;
    border-right: 0;
}



.input-group-btn > .btn + .btn {
    margin-right: -1px;
}

.input-group-btn:not(:last-child) > .btn,
.input-group-btn:not(:last-child) > .btn-group {
    margin-left: -1px;
}

.input-group-btn:not(:first-child) > .btn,
.input-group-btn:not(:first-child) > .btn-group {
    margin-right: -1px;
}

.custom-control {
    padding-right: 1.5rem;
}

    .custom-control + .custom-control {
        margin-right: 1rem;
    }

.custom-control-indicator {
    right: 0;
}

.custom-controls-stacked .custom-control + .custom-control {
    margin-right: 0;
}

.custom-select {
    padding-left: 0.75rem \9;
}

.nav {
    padding-right: 0;
}



    .nav-tabs .nav-item + .nav-item {
        margin-right: 0.2rem;
    }



    .nav-pills .nav-item + .nav-item {
        margin-right: 0.2rem;
    }

.nav-stacked .nav-item + .nav-item {
    margin-right: 0;
}







.navbar-nav .nav-link + .nav-link {
    margin-right: 1rem;
}

.navbar-nav .nav-item + .nav-item {
    margin-right: 1rem;
}

@media (max-width: 543px) {
    .navbar-toggleable .navbar-nav .nav-item {
        margin-right: 0;
    }
}

@media (max-width: 767px) {
    .navbar-toggleable-sm .navbar-nav .nav-item {
        margin-right: 0;
    }
}

@media (max-width: 991px) {
    .navbar-toggleable-md .navbar-nav .nav-item {
        margin-right: 0;
    }
}

.card-link + .card-link {
    margin-right: 1.25rem;
}

.card-blockquote {
    border-right: 0;
}

.breadcrumb-item {
    float: right;
}

.pagination {
    padding-right: 0;
}

.page-item:first-child .page-link {
    margin-right: 0;
}

.page-link {

    margin-right: -1px;
}

.alert-dismissible {
    padding-left: 2rem;
}

    .alert-dismissible .close {
        left: -1rem;
    }

.media-list {
    padding-right: 0;
}

.list-group {
    padding-right: 0;
}

.embed-responsive .embed-responsive-item,
.embed-responsive iframe,
.embed-responsive embed,
.embed-responsive object,
.embed-responsive video {
    right: 0;
}

.close {
    float: left;
}

.tooltip {
    text-align: right;
}

    .tooltip.tooltip-top .tooltip-arrow,
    .tooltip.bs-tether-element-attached-bottom .tooltip-arrow {
        right: 50%;
        margin-right: -5px;
    }

    .tooltip.tooltip-bottom .tooltip-arrow,
    .tooltip.bs-tether-element-attached-top .tooltip-arrow {
        right: 50%;
        margin-right: -5px;
    }

.popover {
    right: 0;
    text-align: right;
}

    .popover.popover-top .popover-arrow,
    .popover.bs-tether-element-attached-bottom .popover-arrow {
        right: 50%;
        margin-right: -11px;
    }

        .popover.popover-top .popover-arrow::after,
        .popover.bs-tether-element-attached-bottom .popover-arrow::after {
            margin-right: -10px;
        }

    .popover.popover-bottom .popover-arrow,
    .popover.bs-tether-element-attached-top .popover-arrow {
        right: 50%;
        margin-right: -11px;
    }

        .popover.popover-bottom .popover-arrow::after,
        .popover.bs-tether-element-attached-top .popover-arrow::after {
            margin-right: -10px;
        }

@media all and (transform-3d), (-webkit-transform-3d) {
    .carousel-inner > .carousel-item.next,
    .carousel-inner > .carousel-item.active.right {
        right: 0;
    }

    .carousel-inner > .carousel-item.prev,
    .carousel-inner > .carousel-item.active.left {
        right: 0;
    }

        .carousel-inner > .carousel-item.next.left,
        .carousel-inner > .carousel-item.prev.right,
        .carousel-inner > .carousel-item.active {
            right: 0;
        }
}

.carousel-inner > .active {
    right: 0;
}

.carousel-inner > .next {
    right: 100%;
}

.carousel-inner > .prev {
    right: -100%;
}

    .carousel-inner > .next.left,
    .carousel-inner > .prev.right {
        right: 0;
    }

.carousel-inner > .active.left {
    right: -100%;
}

.carousel-inner > .active.right {
    right: 100%;
}

/*.carousel-control {
  right: 0;
}

.carousel-control.right {
  left: 0;
  right: auto;
}*/

.carousel-control .icon-prev {
    right: 50%;
    margin-right: -10px;
}

.carousel-control .icon-next {
    left: 50%;
    margin-left: -10px;
}

.carousel-indicators {
    right: 50%;
    padding-right: 0;
    margin-right: -30%;
}

.carousel-caption {
    left: 15%;
    right: 15%;
    z-index: 10;
}

@media (min-width: 544px) {
    .carousel-control .icon-prev {
        margin-right: -15px;
    }

    .carousel-control .icon-next {
        margin-left: -15px;
    }

    .carousel-caption {
        left: 20%;
        right: 20%;
    }
}



/*!
 * Start Bootstrap - Simple Sidebar (https://startbootstrap.com/template-overviews/simple-sidebar)
 * Copyright 2013-2017 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-simple-sidebar/blob/master/LICENSE)
 */



#wrapper {
    position: absolute;
top:5%;
  padding-left: 0;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}

#wrapper.toggled {
  padding-right: 250px;
}

#sidebar-wrapper {
  z-index: 1000;
  position: fixed;
  width: 0;
  height: 100%;
  margin-right: -250px;
  overflow-y: auto;
  background: #000;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
}

#wrapper.toggled #sidebar-wrapper {
  width: 250px;
}

#page-content-wrapper {
  width: 100%;
  position: absolute;
  padding: 15px;
}

#wrapper.toggled #page-content-wrapper {
  position: absolute;
  margin-right: 0px;
}



/* Sidebar Styles */
.navi a {
    border-bottom: 1px solid #0d172e;
    border-top: 1px solid #0d172e;
    color: #ffffff;
    display: block;
    font-size: 17px;
    font-weight: 500;
    padding: 28px 20px;
    text-decoration: none;
}

.navi i {
    margin-right: 15px;
    color: #5584ff;
}



.sidebar-nav a:hover {
    background: #122143 none repeat scroll 0 0;
    border-left: 5px solid #5584ff;
    display: block;
    padding-left: 15px;
}

.sidebar-nav {
  position: absolute;
  top: 2%;
  width: 250px;
  margin-right: 0;
  padding: 0;
  list-style: none;
}

.sidebar-nav li {
  text-indent: 20px;
  line-height: 40px;
}

.sidebar-nav li a {
  display: block;
  text-decoration: none;
  color: #999999;
}

.sidebar-nav li a:hover {
  text-decoration: none;
  color: #fff;
  background: rgba(255, 255, 255, 0.2);
}

.sidebar-nav li a:active, .sidebar-nav li a:focus :not(collapsed) .arrow:before {
  text-decoration: none;
  border-left: 3px solid #d19b3d;
  background-color: #4f5b69;
}

.sidebar-nav>.sidebar-brand {
  height: 65px;
  font-size: 18px;
  line-height: 60px;
}

.sidebar-nav>.sidebar-brand a {
  color: #999999;
}

.sidebar-nav>.sidebar-brand a:hover {
  color: #fff;
  background: none;
}

@media(min-width:768px) {
  #wrapper {
    padding-left: 0;
  }
  #wrapper.toggled {
    padding-right: 250px;
  }
  #sidebar-wrapper {
    width: 0;
  }
  #wrapper.toggled #sidebar-wrapper {
    width: 12%;
  }
  #page-content-wrapper {
    padding: 20px;
    position: relative;
  }
  #wrapper.toggled #page-content-wrapper {
    position: relative;
    margin-right: 0;
  }
}

</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->




        <div id="wrapper" class="toggled">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li>   <a class="nav-link active" href="{{ route('home')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                        Dashboard <span class="sr-only">(current)</span>
                      </a> </li>
                    <li>
                        <a class="nav-link active" href="{{ route('jobs.index')}}">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                          Orders
                        </a>
                      </li>
                    <li>   <a class="nav-link active" href="{{ route('jobs.openOrders')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                        Open Orders
                      </a> </li>
                    <li>   <a class="nav-link active" href="{{ route('customers.index')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                        Customers
                      </a> </li>
                      @if(!Auth::guest())
                      @if(Auth::user()->isAdmin())
                      <li>
                        <a class="nav-link active" href="{{ route('users.index')}}">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                          Users List
                        </a>
                      </li>
                      @endif
                      @endif
                      <li>   <a class="nav-link active" href="{{ route('pages.service')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="12" cy="12" r="3"></circle><path d="M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z"></path></svg>
                        Services
                      </a> </li>
                </ul>
            </div> <!-- /#sidebar-wrapper -->
            <!-- Page Content -->

        </div> <!-- /#wrapper -->
        <!-- Bootstrap core JavaScript -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script> <!-- Menu Toggle Script -->
        <script>
          $(function(){
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });

            $(window).resize(function(e) {
              if($(window).width()<=768){
                $("#wrapper").removeClass("toggled");
              }else{
                $("#wrapper").addClass("toggled");
              }
            });
          });

        </script>





{{-- <nav class="col-md-2 d-none d-md-block bg-light sidebar"  style="position: absolute; top: 10%;">

            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="{{ route('home')}}">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                  Dashboard <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('jobs.index')}}">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                  Orders
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('jobs.openOrders')}}">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                  Open Orders
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('customers.index')}}">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                  Customers
                </a>
              </li>
              @if(!Auth::guest())
              @if(Auth::user()->isAdmin())
              <li class="nav-item">
                <a class="nav-link" href="{{ route('users.index')}}">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                  Users List
                </a>
              </li>
              @endif
              @endif
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                  Integrations
                </a>
              </li>
            </ul>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
              <span>Saved reports</span>
              <a class="d-flex align-items-center text-muted" href="#">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>
              </a>
            </h6>
            <ul class="nav flex-column mb-2">
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                  Current month
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                  Last quarter
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                  Social engagement
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                  Year-end sale
                </a>
              </li>
            </ul>

</nav> --}}

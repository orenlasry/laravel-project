@extends('layouts.newhome')

@section('title', 'Create Order')

@section('content')

    <body>
        <h1>Create Order</h1>
        <form method="post" action="{{ action('JobsController@store') }}">
            @csrf
            <div class="form-group">
                <label for="job_number">Order Number</label>
                <input type="number" class="form-control" name="job_number">
            </div>           
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" name="description" rows="3"></textarea>
              </div>
            <div class="form-group {{ $errors->has('customer_id') ? ' has-error' : '' }}">
                <label for="customer_id">customer</label>
                <div>
                    <select class="form-control" name="customer_id">
                        <option value="" disabled selected hidden>not a regular customer</option>
                        @foreach ($customers as $customer)
                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('customer_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('customer_id') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="client_name">Client name</label>
                    <input type="text" class="form-control" name="client_name">
                </div>
                <div class="form-group">
                    <label for="supply_date">Supply Date</label>
                    <input type="date" class="form-control" name="supply_date">
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    <input type="number" class="form-control" name="price">
                </div>
                <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                    <label for="user_id">User</label>
                    <div>
                        <select class="form-control" name="user_id">
                            <option value="" disabled selected hidden>Choose User</option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('user_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="form-control" name="submit" value="Create Job">
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
        </form>
    </body>
@endsection

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">

    <title>Invoice</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        .text-right {
            text-align: right;
        }
    </style>

</head>
<body class="login-page" style="background: white">
    <div class="w-50 p-3" style=" position: absolute; right:0%; top:0%;">
        <img class=" w-50  rounded float-right" src="{{ url('/images/newlogo.jpeg') }}"width="30%" height="80% " alt="Responsive image">
    </div>
    <div>
        <div class="row">
            <div class="col-xs-7">
                <h4>From: Elraz Furniture Industries Ltd.</h4>
                <strong>Private Company ID : 513634949  </strong><br>
                Geva Binyamin 52 , 90632<br>
                Jerusalem District, Israel<br>
                Phone Office: (02) 5831702 <br>
                Phone Manager: (050) 5254674 <br>
                Fax: (02) 6563332 <br>
                Email: Elraz_ltd@walla.com <br>
                <br>
            </div>
            {{-- <div class="col-xs-4" style=float:right right:63.5%; top:5%;>
                <img src="{{ url('/images/newlogo.jpeg') }}" alt="logo" width="50%" height="50% ">
            </div> --}}
        </div>

        <div style="margin-bottom: 0px">&nbsp;</div>

        <div class="row">
            <div class="col-xs-6">
                <h4>To:</h4>
                <address>
                    @if ($job->customer_id != NULL)
                    <strong>{{$job->customers->name}}</strong>
                    <br>
                    <span>{{$job->customers->email}}</span> <br>
                    <span>{{$job->customers->address}}</span>
                    @else
                    <strong>{{$job->client_name}}</strong>
                    <br>
                    <span></span> <br>
                    @endif   
                </address>
            </div>

            <div class="col-xs-5">
                <table style="width: 100%">
                    <tbody>
                        <tr>
                            <th>Job Number: </th>
                            <td class="text-right">{{$job->job_number}}</td>
                        </tr>
                        <tr>
                            <th> Job Date: </th>
                            <td class="text-right">{{$job->updated_at-> toDateString()}}</td>
                        </tr>
                    </tbody>
                </table>

                <div style="margin-bottom: 0px">&nbsp;</div>

                <table style="width: 100%; margin-bottom: 20px">
                    <tbody>
                        <tr class="well" style="padding: 5px">
                            <th style="padding: 5px"><div> Total Without Tax </div></th>
                            <td style="padding: 5px" class="text-right"><strong> {{$job->price}} </strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        

        <table class="table">
            <thead style="background: #F5F5F5;">
                <tr>
                    <th>Service</th>
                    <th></th>
                    <th class="text-right">Price</th>
                </tr>
            </thead>
            <br>
            <tbody>
                <tr>
                    <td><p>{{$job->description}}</p></td>
                        <td></td>
                        <td class="text-right">{{$job->price}}</td>
                </tr>
            </tbody>
        </table>

            <div class="row">
                <div class="col-xs-6"></div>
                <div class="col-xs-5">
                    <table style="width: 100%">
                        <tbody>
                            <tr class="well" style="padding: 5px">
                                <th style="padding: 5px"><div> Total With Tax </div></th>
                                <td style="padding: 5px" class="text-right"><strong> {{$job->price * 1.17}} </strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div style="margin-bottom: 0px">&nbsp;</div>

            <div class="row">
                <div class="col-xs-8 invbody-terms">
                    Thank you for your business. <br>
                    <br>
                    <h4>Payment Terms</h4>
                    <p></p>
                </div>
            </div>
        </div>

    </body>
    </html>
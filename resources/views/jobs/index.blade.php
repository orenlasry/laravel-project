@extends('layouts.newhome')

@section('title', 'Orders')

@section('content')



    @if (Session::has('notallowed'))
        <div class='alert alert-danger'>
            {{ Session::get('notallowed') }}
        </div>

    @endif

    @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }} </div>
    @endif
    <div><a class="btn btn-primary active" href="{{ route('jobs.create') }}">Add new Order</a></div>

    <h1>{{ $title }}</h1>
    <div class="row">
        <div class="filterable" id="filterable">
            <div class="pull-right">
                <button class="btn btn-default btn-xs btn-filter"> <svg width="20px" height="20px" viewBox="0 0 16 16"
                        class="bi bi-funnel-fill" fill="blue" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M1.5 1.5A.5.5 0 0 1 2 1h12a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.128.334L10 8.692V13.5a.5.5 0 0 1-.342.474l-3 1A.5.5 0 0 1 6 14.5V8.692L1.628 3.834A.5.5 0 0 1 1.5 3.5v-2z" />
                    </svg></button>
            </div>



            <table class="table">
                <thead class="thead-dark">
                    <tr class="filters">
                        <th><input type="text" class="form-control" placeholder="# Order Number" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Customer" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Client Name" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Supply Date" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Price" disabled></th>
                        <th>Status</th>
                        <th><input type="text" class="form-control" placeholder="Created" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Updated" disabled></th>
                        <th>Edit Order</th>
                        <th>Delete Order</th>
                        <th>Download</th>
                    </tr>
                </thead>
                <tbody class="text-white font-weight-bold">
                    <!-- the table data -->
                    @foreach ($jobs as $job)
                        @if (($job->supply_date < $today) & ($job->status_id != 4))
                            <tr class="alert alert-danger">

                            @elseif($job->status_id == 4)
                            <tr class="alert alert-success">

                            @else
                            <tr>
                        @endif

                        <td> {{ $job->job_number }}</td>
                        <td>
                            @if (@isset($job->customer_id))
                                {{ $job->customers->name }}
                            @else
                                Not a regular customer
                            @endif
                        </td>
                        <td> {{ $job->client_name }}</td>
                        <td> {{ $job->supply_date }}</td>
                        <td> {{ number_format($job->price) }}</td>
                        <td>
                            <div class="dropdown">
                                @if (null != App\Status::next($job->status_id))
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        @if (isset($job->status_id))
                                            {{ $job->statuses->name }}
                                        @else
                                            Define Status
                                        @endif
                                    </button>
                                @else
                                    {{ $job->statuses->name }}
                                @endif
                                @if (App\Status::next($job->status_id) != null)
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach (App\Status::next($job->status_id) as $status)
                                            <a class="dropdown-item"
                                                href="{{ route('job.changestatus', [$job->id, $status->id]) }}">{{ $status->name }}</a>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </td>
                        <td> {{ $job->created_at }}</td>
                        <td> {{ $job->updated_at }}</td>
                        <td><a class="btn btn-outline-dark text-white font-weight-bold"
                                href="{{ route('jobs.edit', $job->id) }}" role="button">Edit</a></td>
                        <td><a class="btn btn-outline-danger text-white font-weight-bold" data-toggle="modal"
                                data-target="#delete_job" role="button">Delete</a></td>
                        <td><a class="btn btn-outline-dark text-white font-weight-bold"
                                href="{{ route('job.pdf', $job->id) }}" role="button">Download</a></td>
                        <!-- begin:modal Delete Job -->
                        <div id="delete_job" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <center>
                                        <div class="modal-header">
                                            <h3 class="modal-title">Confirmation</h3><br>
                                        </div>
                                    </center>
                                    <div class="modal-body">
                                        <p>
                                            Are you sure want to Delete this Order?
                                        </p>
                                        <form class="form-horizontal" method="get"
                                            action="{{ route('job.delete', $job->id) }}">
                                            {{ csrf_field() }}
                                            <br>
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <button type="button"
                                                        class="btn btn-hover btn-primary btn-sm text-white font-weight-bold"
                                                        data-dismiss="modal">Cancel</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <input class="btn btn btn-hover btn-danger btn-sm" type="submit"
                                                        value="Yes, Delete">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End:modal Delete Job -->
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            {{ $jobs->links() }}
        </ul>
    </nav>

    <script>
        $(document).ready(function() {

            $('.filterable .btn-filter').click(function() {
                var $panel = $(this).parents('.filterable'),
                    $filters = $panel.find('.filters input'),
                    $tbody = $panel.find('.table tbody');
                if ($filters.prop('disabled') == false) {
                    $filters.prop('disabled', true);
                    $filters.first().focus();
                } else {
                    $filters.val('').prop('disabled', false);
                    $tbody.find('.no-result').remove();
                    $tbody.find('tr').show();
                }
            });

            $('.filterable .filters input').keyup(function(e) {
                /* Ignore tab key */
                var code = e.keyCode || e.which;
                if (code == '9') return;
                /* Useful DOM data and selectors */
                var $input = $(this),
                    inputContent = $input.val().toLowerCase(),
                    $panel = $input.parents('.filterable'),
                    column = $panel.find('.filters th').index($input.parents('th')),
                    $table = $panel.find('.table'),
                    $rows = $table.find('tbody tr');
                /* Dirtiest filter function ever ;) */
                var $filteredRows = $rows.filter(function() {
                    var value = $(this).find('td').eq(column).text().toLowerCase();
                    return value.indexOf(inputContent) === -1;
                });
                /* Clean previous no-result if exist */
                $table.find('tbody .no-result').remove();
                /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
                $rows.show();
                $filteredRows.hide();
                /* Prepend no-result row if all rows are filtered */
                if ($filteredRows.length === $rows.length) {
                    $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="' +
                        $table.find('.filters th').length + '">No result found</td></tr>'));
                }
            });
        });

    </script>
@endsection

@extends('layouts.newhome')

@section('title', 'Edit Order')

@section('content')
    <h1>Edit Order number : {{ $job->job_number }}</h1>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{ action('JobsController@update', $job->id) }}">
        @csrf
        <!-- protect from csrf attack -->
        @method('PATCH')
        <div class="form-group">
            <label for="job_number">Order Number</label>
            <input type="text" class="form-control" name="job_number" value="{{ $job->job_number }}">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" class="form-control" name="description" value="{{ $job->description }}">
        </div>
        <div class="form-group">
            <label for="client_name">Client name</label>
            <input type="text" class="form-control" name="client_name" value="{{ $job->client_name }}"">
            </div>
            <div class=" form-group {{ $errors->has('customer_id') ? ' has-error' : '' }}">
            <label for="customer_id">Customer</label>
            <div>
                <select class="form-control" name="customer_id">
                    @if (@isset($job->customers->id))
                        <option value={{ $job->customers->id }} disabled selected hidden>{{ $job->customers->name }}
                        </option>
                    @else
                        <option value="" disabled selected hidden>not a regular customer</option>
                    @endif
                    @foreach ($customers as $customer)
                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                        <option value="{{ null }}">not a regular customer</option>
                    @endforeach
                </select>

                @if ($errors->has('customer_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('customer_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label for="supply_date">Supply Date</label>
            <input type="date" class="form-control" name="supply_date" value={{ $job->supply_date }}>
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="number" class="form-control" name="price" value={{ $job->price }}>
        </div>
        <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
            <label for="user_id">User</label>
            <div>
                <select class="form-control" name="user_id">
                    <option value={{ $job->users->id }} disabled selected hidden>{{ $job->users->name }}</option>
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>

                @if ($errors->has('user_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('user_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div>
            <input class="btn btn-outline-primary" type="submit" name="submit" value="Update Job">
        </div>
    </form>
@endsection

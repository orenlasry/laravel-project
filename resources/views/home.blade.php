@extends('layouts.newhome')
@section('title', 'home page')

@section('content')
    <!-- Page Content -->
    <div class="container">

        <h1 class="my-4" style="color:red">{{ Auth::user()->name }} Welcome to Your Business</h1>

        <!-- Marketing Icons Section -->
        <div class="row">
            <div class="col-lg-4 mb-4">
                <div class="card h-80">
                    <h4 class="card-header">Orders</h4>
                    <div class="card-body">
                        <p class="card-text">You can manage your order</p>
                        <p>Number of orders : {{ $jobs->count() }}</p>
                        <p>Total revenue : {{ number_format($jobs->sum('price')) }} ₪</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('jobs.index') }}" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4">
                <div class="card h-80">
                    <h4 class="card-header">Open Orders</h4>
                    <div class="card-body">
                        <p class="card-text">You can see and manage your open order</p>
                        <p>Number of open orders : {{ $openJobs->count() }}</p>
                        <p>Total revenue : {{ number_format($openJobs->sum('price')) }} ₪</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('jobs.openOrders') }}" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4">
                <div class="card h-100">
                    <h4 class="card-header">My Orders</h4>
                    <div class="card-body">
                        <p class="card-text">You can manage orders that open by your user</p>
                        <p>Number of open orders : {{ $myjobs->count() }}</p>
                        <p>Total revenue : {{ number_format($myjobs->sum('price')) }} ₪</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 mb-4">
                <div class="card h-90">
                    <h4 class="card-header">Customers</h4>
                    <div class="card-body">
                        <p class="card-text">You can see all your special customers</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('customers.index') }}" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
            @if (!Auth::guest())
                @if (Auth::user()->isAdmin())
                    <div class="col-lg-4 mb-4">
                        <div class="card h-80">
                            <h4 class="card-header">Users</h4>
                            <div class="card-body">
                                <p class="card-text">As Admin you can manage your users</p>
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('users.index') }}" class="btn btn-primary">Learn More</a>
                            </div>
                        </div>

                    </div>
                @endif
            @endif
            <div class="w-50 p-3" style=" position: absolute; left:63.5%; top:63%;">
                <img class=" w-50  rounded float-left" src="{{ url('/images/newlogo.jpeg') }}" alt="Responsive image">
            </div>
        </div>


    </div>
    <!-- /.row -->
@endsection

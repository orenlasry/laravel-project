@extends('layouts.newhome')
@section('title','service')

@section('content')
@if (Session::has('message'))
<div class="alert alert-success">{{Session::get('message')}} </div>
@endif
        <div class="row">
            <div class="col-md-12">
                <h1>Contact Us</h1>
                <hr>
                <form action="{{ url('service') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label name="email">Email:</label>
                        <input id="email" name="email" value= {{Auth::user()->email}} class="form-control">
                    </div>

                    <div class="form-group">
                        <label name="subject">Subject:</label>
                        <input id="subject" name="subject" class="form-control">
                    </div>

                    <div class="form-group">
                        <label name="message">Message:</label>
                        <textarea id="message" name="message" class="form-control" placeholder="Type your message here..."></textarea>
                    </div>

                    <input type="submit" value="Send Message" class="btn btn-success">
                </form>
            </div>
        </div>
@endsection

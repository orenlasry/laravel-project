<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

use Illuminate\Support\Facades\DB;

class PagesController extends Controller {

	public function getIndex() {
		$posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
		return view('pages.welcome')->withPosts($posts);
	}

	// public function getAbout() {
	// 	$first = 'Yael';
	// 	$last = 'Lasry';

	// 	$fullname = $first . " " . $last;
	// 	$email = 'laravel.project.jce@gmail.com';
	// 	$data = [];
	// 	$data['email'] = $email;
	// 	$data['fullname'] = $fullname;
	// 	return view('pages.about')->withData($data);
	// }

	public function getContact() {
		return view('pages.service');
	}

	public function postContact(Request $request) {
		$this->validate($request, [
			'email' => 'required|email',
			'subject' => 'min:3',
			'message' => 'min:10']);

		$data = array(
			'email' => $request->email,
			'subject' => $request->subject,
			'bodyMessage' => $request->message
			);

		Mail::send('mail.service-email', $data, function($message) use ($data){
			$message->from($data['email']);
			$message->to('laravel.project.jce@gmail.com');
			$message->subject($data['subject']);
		});

		Session::flash('success', 'Your Email was Sent!');

		return back()->with('message','Your Service Request was Sent!');;
	}


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\User;
use App\Status;
use App\Customer;
use \PDF;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::orderBy('supply_date')->paginate(10);
        $users = User::all();
        $today =  Carbon::today();
        $customers = Customer::all();
        $statuses = Status::all();
        $title= "Orders";
        return view('jobs.index', compact('jobs','users','today','customers','statuses','title'));
    }

    public function changeStatus($jid,$sid){
        $job = Job::findOrFail($jid);
        /*$uid =  Auth::id();*/
        if(Gate::authorize('change-status',Auth::user()))
        {
            $fromStage = $job->status_id;
            $check =  Status::allowed($sid,$fromStage);
        if ($check == TRUE){
            $job->status_id = $sid;
            $job->save();
            return back();}
        }
        return redirect('jobs');
        }

        public function changeUser($jid,$cid = null){
            /*Gate::authorize('assign-user',Auth::user());*/
            $job = Job::findOrFail($jid);
            $job->customer_id = $cid;
            $job->save();
            return back();
        }
        public function openOrders(){
            $jobs = Job::where('status_id','<>','4')->paginate(10);
            $users = User::all();
            $today =  Carbon::today();
            $customers = Customer::all();
            $statuses = Status::all();
            $title= "Open orders";
           return view('jobs.index', compact('jobs','users','today','customers','statuses','title'));
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jobs = Job::all();
        $users = User::all();
        $statuses = Status::all();
        $customers = Customer::all();
        return view('jobs.create', compact('jobs','users','customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $job = new Job();
        $this-> validate($request,[
            'supply_date' => 'required',
            'client_name' => 'required',
            'job_number' =>'required',
            'price' =>'required',
            'user_id' =>'required'

        ]);
        $job->customer_id =  $request ->customer_id;
        $job->create($request ->all());
        return redirect('jobs')->with('message','The Order has been Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::findOrFail($id); // check if object exists
        $users = User::all();
        $customers = Customer::all();
        return view('jobs.edit', compact('job', 'users','customers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'supply_date' => 'required',
            'client_name' => 'required',
            'job_number' =>'required',
            'price' =>'required',

        ]);
        $job = Job::findOrFail($id);
        $job -> update($request->all());// update all data
        return redirect()->route('jobs.index')->with('success', 'Data Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = Job::findOrFail($id);
        $job->delete();
        return redirect('jobs')->with('message','The Order has been Removed');

    }

    public function download($id)
    {
        $job = Job::findOrFail($id);
        $customers = Customer::all();
        $pdf = PDF::loadView('jobs.invoice-pdf',compact('job','customers'));
        return $pdf->download('receipt '.$job->job_number.'.pdf');

    }
}

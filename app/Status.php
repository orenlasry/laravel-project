<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    public function jobStatus(){
        return $this->hasMany('App\Job');
    }

    public static function next($status_id) {
        $nextstages = DB::table('nextstages')->where('from',$status_id)->pluck('to');
        return self::find($nextstages)->all();
    }
    public static function allowed($to,$from){
        if ($from == 1) {
            if ($to == 2){
                return TRUE;
            }
        }
        elseif ($from == 2) {
            if ($to == 3){
                return TRUE;
            }
        }
        elseif ($from == 3) {
            if ($to == 4){
                return TRUE;
            }
        }
        else 
            return FALSE;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name','customer_number','phone_num','email', 'address'];

    public function jobs(){
        return $this->hasMany('App\Job','job_id');
    }

}

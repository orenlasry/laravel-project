<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = ['client_name','job_number','description','supply_date','price', 'user_id','customer_id'];

    // Connections
    public function users(){
        return $this->belongsTo('App\User','user_id');
    }

    public function statuses(){
        return $this->belongsTo('App\Status','status_id');
    }

    public function customers(){
        return $this->belongsTo('App\Customer','customer_id');
    }

}

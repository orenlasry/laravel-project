<?php

use Illuminate\Database\Seeder;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            ['name' => 'Order received',
            'created_at' => now(),
            'updated_at' => now(),],
            ['name' => 'Order in progress',
            'created_at' => now(),
            'updated_at' => now(),],
            ['name' => 'Order sent',
            'created_at' => now(),
            'updated_at' => now(),],
            ['name' => 'Order completed',
            'created_at' => now(),
            'updated_at' => now(),]]); 
    }
}

